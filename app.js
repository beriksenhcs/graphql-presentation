'use strict';

require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const apolloServer = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');
const PORT = 3030;

(async function () {
  await require('./data/redisStore').initialize();

  var app = express();

  for (let i = 1; i <= 5; i++) {
    let typeDefs = require(`./schema/schema.v${i}`);
    let resolvers = require(`./resolvers/resolvers.v${i}`);
    let schema = makeExecutableSchema({ typeDefs, resolvers });

    app.use(`/graphql/v${i}`, bodyParser.json(), apolloServer.graphqlExpress((req, res) => ({
      schema
    })));

    app.use(`/graphiql/v${i}`, apolloServer.graphiqlExpress({ endpointURL: `/graphql/v${i}` }));
  }

  app.use('/health', (req, res) => {
    res.status(200).json({
      isSuccessful: true
    });
    return;
  })

  app.listen(PORT);
})();