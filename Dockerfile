FROM node:9.1.0-alpine

WORKDIR /app

EXPOSE 3030

ENV REDISHOST=hcs-foundation_redis

ENTRYPOINT ["npm", "start"]

# Only add package.json first and then run npm install, so that these steps can be cached and only re-executed if dependencies change.
COPY package.json /app
RUN cd /app && npm install

# Copy the rest of the app code
COPY . /app