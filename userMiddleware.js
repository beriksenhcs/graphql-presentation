module.exports = (req, res, next) => {
  let userHeader = req.headers['user'];
  res.locals.user = {
  	name: userHeader
  };
  next();
};