const redis = require('redis');
const client = redis.createClient({
  host: process.env.REDISHOST
});
const { promisify } = require('util');
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const delAsync = promisify(client.del).bind(client);
const loremIpsum = require('lorem-ipsum');

async function initialize() {
  for (var i = 1; i <= 5; i++) {
    await setAsync(`${i}-description`, loremIpsum({ count: 4, units: 'sentences' }));
  }
  for (var i = 6; i <= 10; i++) {
    await delAsync(`${i}-description`);
  }
}

module.exports = {
  initialize,
  get: getAsync,
  set: setAsync
}