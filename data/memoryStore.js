const { orderBy } = require('lodash');
const loremIpsum = require('lorem-ipsum');

class Requisition {
	constructor(id, name) {
		this.id = id;
    this.name = name;
    this.tags = [];

    let isOdd = (Number(id) % 2) > 0;
    this.tags.push(isOdd ? "odd" : "even");
    this.tags.push(loremIpsum({ count: 1, units: 'words' }));
	}
}

let requisitions = [
  new Requisition("1", "Requisition 1"),
  new Requisition("2", "Requisition 2"),
  new Requisition("3", "Requisition 3"),
  new Requisition("4", "Requisition 4"),
  new Requisition("5", "Requisition 5"),
];

module.exports = {
  requisitions,
  addRequisition: (name) => {
    let newId = (Number(orderBy(requisitions, ['id'], ['desc'])[0].id) + 1).toString();
    let newReq = new Requisition(newId, name);
    requisitions.push(newReq);
    return newReq;
  }
}