module.exports = `
	type Requisition {
		id: ID!,
		name: String!
	}

	type Query {
		requisitions: [Requisition!]
	}
`;