module.exports = `
	type Requisition {
		id: ID!,
		name: String!,
    tags(prefix: String): [String!]
	}

	type Query {
		requisitions: [Requisition!],
		requisitionById(id: ID!): Requisition
	}
`;