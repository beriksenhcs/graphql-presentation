module.exports = `
	type Requisition {
		id: ID!,
		name: String!,
    tags(prefix: String): [String!],
    description: String
	}

	type Query {
		requisitions: [Requisition!],
		requisitionById(id: ID!): Requisition,
    host: String!
	}

  input RequisitionInput {
    name: String!
    description: String
  }
  
  type Mutation {
    addRequisition(requisition: RequisitionInput!): Requisition
  }
`;