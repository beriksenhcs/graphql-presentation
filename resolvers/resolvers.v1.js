const memoryStore = require('../data/memoryStore');

module.exports = {
	Query: {
		requisitions: () => {
			return memoryStore.requisitions;
		}
	}
}