const memoryStore = require('../data/memoryStore');
const { startsWith } = require('lodash');
const redisStore = require('../data/redisStore');

module.exports = {
	Query: {
		requisitions: () => {
			return memoryStore.requisitions;
		},

		requisitionById: (_, args) => {
			return memoryStore.requisitions.filter((r) => {
				return r.id === args.id;
      })[0];
		}
  },

  Requisition: {
    tags: (req, args) => {
      if (!!args.prefix) {
        return req.tags.filter((t) => startsWith(t, args.prefix));
      }
      return req.tags;
    },
    description: (req) => {
      return redisStore.get(`${req.id}-description`);
    }
  },

  Mutation: {
    addRequisition: (_, args) => {
      let newRequisition = memoryStore.addRequisition(args.requisition.name);
      if (!!args.requisition.description) {
        return redisStore.set(`${newRequisition.id}-description`, args.requisition.description)
          .then(() => newRequisition);
      }
      return newRequisition;
    }
  }
}