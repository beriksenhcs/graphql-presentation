const memoryStore = require('../data/memoryStore');
const { startsWith } = require('lodash');

module.exports = {
	Query: {
		requisitions: () => {
			return memoryStore.requisitions;
		},

		requisitionById: (_, args) => {
			return memoryStore.requisitions.filter((r) => {
				return r.id === args.id;
      })[0];
		}
  },

  Requisition: {
    tags: (req, args) => {
      if (!!args.prefix) {
        return req.tags.filter((t) => startsWith(t, args.prefix));
      }
      return req.tags;
    }
  }
}